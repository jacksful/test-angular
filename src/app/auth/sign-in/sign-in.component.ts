import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { User } from '../../models/user';
// import { Store } from '@ngrx/store';
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  form: any = FormGroup;
  constructor(private formBuilder: FormBuilder,

    ) { }

  LoginForm = new FormGroup({
    userName: new FormControl('', 
    [
      Validators.required, 
      Validators.minLength(2)
    ]),
    password: new FormControl('', 
    [
      Validators.required, 
      Validators.minLength(6)
    ])
  });

  ngOnInit(): void {
    
  }

  get userName() {
    return this.LoginForm.get('userName');
  }

  get password() {
    return this.LoginForm.get('password');
  }

  signInsubmit(){
    console.log(this.LoginForm);
  }

}
