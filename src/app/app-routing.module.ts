import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  {
    path: 'employee',
    loadChildren: () => import(`./employee/employee.module`).then(
      module => module.EmployeeModule
    )
  },
  {
    path: '',
    loadChildren: () => import(`./dash-board/dash-board.module`).then(
      module => module.DashBoardModule
    )
  },
  {
    path: 'auth',
    loadChildren: () => import(`./auth/auth.module`).then(
      module => module.AuthModule
    )
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
