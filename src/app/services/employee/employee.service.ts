import { Injectable } from '@angular/core';
import { AllApiService } from '../all-api.service';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError, Observable, tap, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(
    private allapi: AllApiService,
    private httpclient: HttpClient,
    
  ) { }

  getEmployeeList() {
    const url = this.allapi.employees;
    return this.httpclient.get(url);
  }
}
