import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AllApiService {

  baseURL = 'https://dummy.restapiexample.com/api/v1/';

  employees = this.baseURL + 'employees';

}
