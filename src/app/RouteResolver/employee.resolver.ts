import { Injectable } from '@angular/core';
import { EmployeeService } from '../services/employee/employee.service';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeResolver implements Resolve<any> {
  constructor(
    private employeeService: EmployeeService
  ) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    return this.employeeService.getEmployeeList();
  }
}
