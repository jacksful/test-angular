import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { getEmployeeData } from '../state/employee.selectors';
import { EmployeeState } from '../state/employee.state';
@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  employeeData = [];
  employeeFromData: Observable<EmployeeState[]> | any;
  constructor(
    private actvRoute: ActivatedRoute,
    private store: Store<{employeeData: {employeeData: object}}>
  ) { }

  ngOnInit(): void {
    console.log("this.actvRoute.snapshot.data");
    this.employeeData = this.actvRoute.snapshot.data['employeeList'].data;
    console.log(this.employeeData);
    this.employeeFromData = this.store.select(getEmployeeData);
      console.log(this.employeeFromData);

  }

}
