import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { Store } from '@ngrx/store';
import { createEmployee } from '../state/employee.actions';
import {getEmployeeData} from '../state/employee.selectors'
import { Observable } from 'rxjs';
import { ofType } from '@ngrx/effects';
import { EmployeeState } from '../state/employee.state';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {
  datePickerConfig: Partial<BsDatepickerConfig>;
  title = 'newMat';
  maxDate = new Date();

 

  isLinear = true;
  employeeData: Observable<EmployeeState[]> | any;
  basicInfo = true;
  skilInfo = false;
  doneInfo = false;
  firstFormGroup = new FormGroup({
    firstName: new FormControl('', 
    [
      Validators.required,
      Validators.minLength(2)
    ]),
    lastName: new FormControl('', 
    [
      Validators.required
    ]),
    dob:  new FormControl('', 
    [
      Validators.required
    ]),
    phone:  new FormControl('', 
    [
      Validators.required,
      Validators.pattern("^((\\+91-?)|0)?[0-9]{11}$")
    ]),
    
  });

  secondFormGroup = new FormGroup({
    salary: new FormControl('', 
    [
      Validators.required
    ]),
  });

  skillLabels = ['Beginner','Intermediate', 'Advanced'];
  skillFormGroup = new FormGroup({
    skillName: new FormControl('', 
    [
      Validators.required
    ]),
    experienceInYear: new FormControl('', 
    [
      Validators.required
    ]),
    skillLabel:  new FormControl('', 
    [
      Validators.required
    ]),
    
  });
 
  constructor(private store: Store<{employeeData: {employeeData: object}}>) {
    this.maxDate.setDate(this.maxDate.getDate() - 1);
    this.datePickerConfig = Object.assign({},
      { containerClass: 'theme-dark-blue' },
      { showWeekNumbers: false },
      { maxDate: this.maxDate }
    );
    
  }
  
  ngOnInit() {
    
    this.employeeData = this.store.select(getEmployeeData);
  }
  
  submit(){
      // console.log(this.firstFormGroup.value);
      // console.log(this.secondFormGroup.value);
      let finalData = {
        ...this.firstFormGroup.value,
        ...this.skillFormGroup.value
      }
      this.store.dispatch(createEmployee(finalData));
      
      this.employeeData = this.store.select(getEmployeeData);
      console.log(this.employeeData);
     
  }

  get phone() {
    return this.firstFormGroup.get('phone');
  }

  get firstName() {
    return this.firstFormGroup.get('firstName');
  }

  tabChange(index:any){
    if(index == '1'){
      this.basicInfo = true;
      this.skilInfo = false;
      this.doneInfo = false;
    }else if(index == '2'){
      this.basicInfo = false;
      this.skilInfo = true;
      this.doneInfo = false;
    }else{
      this.basicInfo = false;
      this.skilInfo = false;
      this.doneInfo = true;
    }
  }

  get skillName() {
    return this.skillFormGroup.get('skillName');
  }

  get experienceInYear() {
    return this.skillFormGroup.get('experienceInYear');
  }

  get skillLabel() {
    return this.skillFormGroup.get('skillLabel');
  }


}
