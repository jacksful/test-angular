import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeSkilInformationComponent } from './employee-skil-information.component';

describe('EmployeeSkilInformationComponent', () => {
  let component: EmployeeSkilInformationComponent;
  let fixture: ComponentFixture<EmployeeSkilInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeSkilInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeSkilInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
