import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { Store } from '@ngrx/store';
import {getEmployeeData} from '../state/employee.selectors'
import { Observable } from 'rxjs';
import { EmployeeState } from '../state/employee.state';

@Component({
  selector: 'app-employee-skil-information',
  templateUrl: './employee-skil-information.component.html',
  styleUrls: ['./employee-skil-information.component.css']
})
export class EmployeeSkilInformationComponent implements OnInit {
  title = 'newMat';
  maxDate = new Date(new Date().getTime() - 24*60*60*1000);
  isLinear = true;
  employeeData: Observable<EmployeeState[]> | any;
  skillLabels = ['Beginner','Intermediate', 'Advanced'];
  skillFormGroup = new FormGroup({
    skillName: new FormControl('', 
    [
      Validators.required
    ]),
    experienceInYear: new FormControl('', 
    [
      Validators.required
    ]),
    skillLabel:  new FormControl('', 
    [
      Validators.required
    ]),
    
  });


 
  constructor(private store: Store<{employeeData: {employeeData: object}}>) {
  }
  
  ngOnInit() {
    this.employeeData = this.store.select(getEmployeeData);
  }

  get skillName() {
    return this.skillFormGroup.get('skillName');
  }

  get experienceInYear() {
    return this.skillFormGroup.get('experienceInYear');
  }

  get skillLabel() {
    return this.skillFormGroup.get('skillLabel');
  }


  

}
