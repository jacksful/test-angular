import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { Store } from '@ngrx/store';
import { createEmployee } from '../state/employee.actions';
import {getEmployeeData} from '../state/employee.selectors'
import { Observable } from 'rxjs';
import { ofType } from '@ngrx/effects';
import { EmployeeState } from '../state/employee.state';

@Component({
  selector: 'app-employee-basic-information',
  templateUrl: './employee-basic-information.component.html',
  styleUrls: ['./employee-basic-information.component.css']
})
export class EmployeeBasicInformationComponent implements OnInit {
  datePickerConfig: Partial<BsDatepickerConfig>;
  title = 'newMat';
  maxDate = new Date(new Date().getTime() - 24*60*60*1000);
  isLinear = true;
  employeeData: Observable<EmployeeState[]> | any;

  firstFormGroup = new FormGroup({
    firstName: new FormControl('', 
    [
      Validators.required,
      Validators.minLength(2)
    ]),
    lastName: new FormControl('', 
    [
      Validators.required
    ]),
    dob:  new FormControl('', 
    [
      Validators.required
    ]),
    phone:  new FormControl('', 
    [
      Validators.required,
      Validators.pattern("^((\\+91-?)|0)?[0-9]{11}$")
    ]),
    
  });

  constructor(private store: Store<{employeeData: {employeeData: object}}>) {
    
    this.datePickerConfig = Object.assign({},
      { containerClass: 'theme-dark-blue' },
      { showWeekNumbers: false },
      { maxDate: this.maxDate }
    );
    
  }
  
  ngOnInit() {
    this.maxDate.setDate(this.maxDate.getDate() + 7);
    this.employeeData = this.store.select(getEmployeeData);
  }

  get phone() {
    return this.firstFormGroup.get('phone');
  }

  get firstName() {
    return this.firstFormGroup.get('firstName');
  }
}
