import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EmployeeRoutingModule } from './employee-routing.module';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
import {MatStepperModule} from '@angular/material/stepper';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { StoreModule } from '@ngrx/store';
import {MatSelectModule} from '@angular/material/select';
import { EmployeeBasicInformationComponent } from './employee-basic-information/employee-basic-information.component';
import { EmployeeSkilInformationComponent } from './employee-skil-information/employee-skil-information.component';
@NgModule({
  declarations: [
    EmployeeListComponent,
    CreateEmployeeComponent,
    EmployeeBasicInformationComponent,
    EmployeeSkilInformationComponent
  ],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatListModule,
    MatStepperModule,
    BsDatepickerModule.forRoot()
  ]
})
export class EmployeeModule { }
