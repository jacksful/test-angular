import {
    createEmployee
} from './employee.actions';
  import { EmployeeState, initialState } from './employee.state';
  import { Action, createReducer, on } from '@ngrx/store';
  
  const _employeeReducer = createReducer(
    initialState,
    on(createEmployee, (state) => {
      return {
        ...state,
        employeeData: state.employeeData,
      };
    }),
    
  );
  
  export function employeeReducer(state: EmployeeState | undefined, action: Action) {
    return _employeeReducer(state, action);
  }


