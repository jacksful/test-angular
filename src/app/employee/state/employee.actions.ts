import { createAction, props } from '@ngrx/store';
export const createEmployee = createAction(
  'createEmployee',
  props<{ employeeData: object }>()
);
