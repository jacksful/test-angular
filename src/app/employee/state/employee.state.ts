export interface EmployeeState {
    employeeData: object;
}

export const initialState: EmployeeState = {
    employeeData: {}
};