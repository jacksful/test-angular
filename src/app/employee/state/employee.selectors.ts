import { EmployeeState } from './employee.state';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export const Employee_STATE_NAME = 'employeeData';

const getEmployeeState = createFeatureSelector<EmployeeState>(Employee_STATE_NAME);

export const getEmployeeData = createSelector(getEmployeeState, (state) => {
  return state.employeeData;
});
