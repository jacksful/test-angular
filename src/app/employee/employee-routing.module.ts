import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeeResolver } from '../RouteResolver/employee.resolver';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
const routes: Routes = [
  {
    path: 'list',
    component: EmployeeListComponent,
    resolve: {employeeList: EmployeeResolver}
  },
  {
    path: 'create',
    component: CreateEmployeeComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
